import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders, NgModule } from '@angular/core';
import{ RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'; 

import { StoreModule as NgRxStoreModule, ActionReducerMap} from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import {DestinosViajesState, reducerDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects} from './models/destinos-viajes-states.model';
import { StoreDevtools } from '@ngrx/store-devtools'



const routes: Routes = [
{path:'', redirectTo: 'home', pathMatch: 'full'},
{path:'home', component:ListaDestinosComponent},
{path:'destino', component:DestinoDetalleComponent}
];
export interface AppState{
  destinos: DestinosViajesState;
}
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};
const reducersInitialState = { 
destinos: initializeDestinosViajesState()};

export const routing: ModuleWithProviders<any> = RouterModule.forRoot(routes);

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent


    
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers,{initialState: reducersInitialState }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [DestinosApiClient, ],
  bootstrap: [AppComponent]
})
export class AppModule{}

