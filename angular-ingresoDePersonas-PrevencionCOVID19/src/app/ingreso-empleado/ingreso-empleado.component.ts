import { Component, OnInit, Input, HostBinding , Output, EventEmitter} from '@angular/core';
import {ingresoEmpleado} from './../models/ingresoEmpleado.models';
@Component({
  selector: 'app-ingreso-empleado',
  templateUrl: './ingreso-empleado.component.html',
  styleUrls: ['./ingreso-empleado.component.css']
})
export class IngresoEmpleadoComponent implements OnInit {
	@Input() ingreso:ingresoEmpleado;
	@HostBinding('style.backgroundColor') backgroundColor = 'red';
	@Output() clicked:EventEmitter<ingresoEmpleado>;
  
  constructor() {
  this.clicked=new EventEmitter(); }

  ngOnInit(): void {
  }
  marcar(){
  this.clicked.emit(this.ingreso);
  return false;
  }
}
