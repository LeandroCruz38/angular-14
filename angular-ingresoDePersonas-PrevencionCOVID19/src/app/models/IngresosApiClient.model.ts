import { BehaviorSubject, Subject } from 'rxjs';
import {ingresoEmpleado} from './ingresoEmpleado.models';

export class ingresosApiClient{
current: Subject<ingresoEmpleado>= new BehaviorSubject<ingresoEmpleado>(null);
ingresos: ingresoEmpleado[]
constructor(){
    this.ingresos=[];
}
add(d:ingresoEmpleado){
    this.ingresos.push(d);
}
getAll():ingresoEmpleado[]{
    return this.ingresos;
}

elegir(d: ingresoEmpleado){
    this.ingresos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next;

}
subscribeOnChange(fn){
    this.current.subscribe(fn);
}



}