import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ingresoEmpleado } from '../models/ingresoEmpleado.models';
 
@Component({
  selector: 'app-forms-ingreso-personas',
  templateUrl: './forms-ingreso-personas.component.html',
  styleUrls: ['./forms-ingreso-personas.component.css']
})
export class FormsIngresoPersonasComponent implements OnInit {
@Output() onItemAdded: EventEmitter<ingresoEmpleado>;
fg: FormGroup;
  constructor(fb: FormBuilder) {
    this.onItemAdded= new EventEmitter();
    this.fg =fb.group({
      apellido:[''],
      temperatura:[''],
    });
   }

  ngOnInit(): void {
  }
  guardar(apellido:string, temp:string):boolean{
const d=new ingresoEmpleado(apellido, temp);
this.onItemAdded.emit(d);
return false;
  }

}
