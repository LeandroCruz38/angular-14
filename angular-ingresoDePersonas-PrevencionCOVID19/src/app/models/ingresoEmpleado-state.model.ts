import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ingresoEmpleado } from './ingresoEmpleado.models';
import { ingresosApiClient } from './IngresosApiclient.model';
import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

// ESTADO
export interface ingresoEmpleadoState {
    items: ingresoEmpleado[];
    loading: boolean;
    tempMasAlta: ingresoEmpleado;
}

export function intializeIngresoEmpleadosState() {
  return {
    items: [],
    loading: false,
    tempMasAlta: null
  };
}

// ACCIONES
export enum ingresoEmpleadosActionTypes {
  NUEVO_EMPLEADO = '[Ingreso Empleados] Nuevo',
  ELEGIDO_MASTEMP = '[Ingreso Empleado] Temperatura',
  VOTE_UP = '[Ingreso Empleado] Vote Up',
  VOTE_DOWN = '[Ingreso Empleado] Vote Down',
  INIT_MY_DATA = '[Ingreso Empleado] Init My Data'
}

export class NuevoEmpleadoAction implements Action {
  type = ingresoEmpleadosActionTypes.NUEVO_EMPLEADO;
  constructor(public empleado: ingresoEmpleado) {}
}

export class ElegidoMasTempAction implements Action {
  type = ingresoEmpleadosActionTypes.ELEGIDO_MASTEMP;
  constructor(public empleado: ingresoEmpleado) {}
}

export class VoteUpAction implements Action {
  type = ingresoEmpleadosActionTypes.VOTE_UP;
  constructor(public empleado: ingresoEmpleado) {}
}

export class VoteDownAction implements Action {
  type = ingresoEmpleadosActionTypes.VOTE_DOWN;
  constructor(public empleado: ingresoEmpleado) {}
}

export class InitMyDataAction implements Action {
  type = ingresoEmpleadosActionTypes.INIT_MY_DATA;
  constructor(public empleado: string[]) {}
}

export type ingresoEmpleadosActions = NuevoEmpleadoAction | ElegidoMasTempAction
  | VoteUpAction | VoteDownAction | InitMyDataAction;

// REDUCERS
export function reducerIngresoEmpleado(
  state: ingresoEmpleadoState,
  action: ingresoEmpleadosActions
): ingresoEmpleadoState {
  switch (action.type) {
    case ingresoEmpleadosActionTypes.INIT_MY_DATA: {
      const empleado: string[] = (action as InitMyDataAction).empleado;
      return {
          ...state,
          items: empleado.map((d) => new ingresoEmpleado(d, ''))
        };
    }
    case ingresoEmpleadosActionTypes.NUEVO_EMPLEADO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoEmpleadoAction).empleado ]
        };
    }
    case ingresoEmpleadosActionTypes.ELEGIDO_MASTEMP: {
        state.items.forEach(x => x.setSelected(false));
        const fav: ingresoEmpleado = (action as ElegidoMasTempAction).empleado;
        fav.setSelected(true);
        return {
          ...state,
          tempMasAlta: fav
        };
    }
    case ingresoEmpleadosActionTypes.VOTE_UP: {
        const d: ingresoEmpleado = (action as VoteUpAction).empleado;
        d.voteUp();
        return { ...state };
    }
    case ingresoEmpleadosActionTypes.VOTE_DOWN: {
        const d: ingresoEmpleado = (action as VoteDownAction).empleado;
        d.voteDown();
        return { ...state };
    }
  }
  return state;
}

// EFFECTS
@Injectable()
export class ingresoEmpleadosEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(ingresoEmpleadosActionTypes.NUEVO_EMPLEADO),
    map((action: NuevoEmpleadoAction) => new ElegidoMasTempAction(action.empleado))
  );

  constructor(private actions$: Actions) {}
}