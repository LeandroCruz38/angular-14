import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsIngresoPersonasComponent } from './forms-ingreso-personas.component';

describe('FormsIngresoPersonasComponent', () => {
  let component: FormsIngresoPersonasComponent;
  let fixture: ComponentFixture<FormsIngresoPersonasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsIngresoPersonasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsIngresoPersonasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
