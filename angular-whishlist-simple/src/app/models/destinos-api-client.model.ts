import { destinoViaje } from './destino-Viaje.models';
import{ Subject, BehaviorSubject} from 'rxjs';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-states.model';
import { Injectable } from '@angular/core';
@Injectable()
export class DestinosApiClient {
	
	constructor(private store: Store<AppState>) {
      
	}
	add(d:destinoViaje){
	  this.store.dispatch(new NuevoDestinoAction(d));
	}

    
    elegir(d:destinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
    }
   
}