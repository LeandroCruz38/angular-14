import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {ingresoEmpleado } from './../models/ingresoEmpleado.models';
import {ingresosApiClient } from './../models/IngresosApiClient.model';
@Component({
  selector: 'app-listado-ingresos',
  templateUrl: './listado-ingresos.component.html',
  styleUrls: ['./listado-ingresos.component.css']
})
export class ListadoIngresosComponent implements OnInit {

  updates:string[];
  constructor(private ingresosApiClient: ingresosApiClient) {
 
  
  this.updates=[];
  this.ingresosApiClient.subscribeOnChange((d:ingresoEmpleado) => {if (d!=null){this.updates.push('Se ha elegido a' +d.apellido);}})
}
  ngOnInit(): {
    
  } 
  
agregado(e:ingresoEmpleado) {
this.ingresosApiClient.add(e);
//this.onItemAdded.emit(e);
}
elegido(d:ingresoEmpleado){
	
this.ingresosApiClient.elegir(d);
}  
}
