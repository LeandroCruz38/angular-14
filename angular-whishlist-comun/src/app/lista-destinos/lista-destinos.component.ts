import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinosApiClient } from '../models/destino-api-client.model';

import { DestinoViaje } from '../models/destino-Viaje.models';
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;

  constructor(public destinosApiClient: DestinosApiClient) {
    this.onItemAdded=new EventEmitter();
   }

  ngOnInit(): void {
  }
  agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
    //this.onItemAdded.emit(d);
    
    
  
    return false;
  }
  elegido(d:DestinoViaje){
    this.destinosApiClient.getAll().forEach(x=>x.setSelected(false));
    d.setSelected(true);
  }
}
