import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListadoIngresosComponent } from './listado-ingresos/listado-ingresos.component';
import { IngresoEmpleadoComponent } from './ingreso-empleado/ingreso-empleado.component';
import { FormsIngresoPersonasComponent } from './forms-ingreso-personas/forms-ingreso-personas.component';
import { ingresoEmpleadoState, intializeIngresoEmpleadosState, reducerIngresoEmpleado } from './models/ingresoEmpleado-state.model';
import { ActionReducerMap } from '@ngrx/store';


export interface AppState{
  ingresos: ingresoEmpleadoState;
}
const reducers: ActionReducerMap<AppState>={
  ingresos:reducerIngresoEmpleado
};
let reducerInitialState={
  ingreso: intializeIngresoEmpleadosState()
};


@NgModule({
  declarations: [
    AppComponent,
    ListadoIngresosComponent,
    IngresoEmpleadoComponent,
    FormsIngresoPersonasComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
    NgRXStoreM  < ++) {
      const e ];
      
    }
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
