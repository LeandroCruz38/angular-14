import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {destinoViaje} from './../models/destino-Viaje.models';
import {DestinosApiClient} from './../models/destinos-api-client.model';
import {DestinosViajesState} from  './../models/destinos-viajes-states.model';
import {Store} from '@ngrx/store';

import {AppState} from './../app.module';
import {ElegidoFavoritoAction,NuevoDestinoAction} from '../models/destinos-viajes-states.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
@Output() onItemAdded:EventEmitter<destinoViaje>;
updates: string[];
all;
  constructor(private destinosApiClient:DestinosApiClient, private store: Store<AppState>) { 
this.onItemAdded= new EventEmitter();
this.updates=[];
this.store.select(state => state.destinos.favorito).subscribe(d=>{ if(d!=null){this.updates.push('Se ha elegido a' + d.nombre);}});
store.select(state=>state.destinos.items).subscribe(items=>this.all=items);
  }
  
  ngOnInit(): void {

  }
agregado(d:destinoViaje) {
this.destinosApiClient.add(d);
this.onItemAdded.emit(d);
	
	

}

elegido(e:destinoViaje){
	this.destinosApiClient.elegir(e);
	
}
getAll(){

}
}
